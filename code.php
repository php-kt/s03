<?php
//Objects as Variables

$buildingObj = (object) [
	'name' => 'Caswynn Building',
	'floors' => 8,
	'address' => (object) [
		'barangay' => 'Sacred Heart',
		'city' => 'Quezon City',
		'country' => 'Philippines'
	]

];

//Objects from Classes
//properties and attributes public + $properties
	//Properties
class Building {
	// disable ang access and inheratance of the proeprty pag nakaprivate
	//private $name;

	protected $name;
	protected $floors;
	protected $address;

	//Constructor
	//two _
	public function __construct($name, $floors, $address) {
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	//methods
	public function printName() {
		return "The name of the building is $this->name";
	}

	//Act4, both classes wull use the getters and setters

	public function getName(){
		return $this->name;
	}

	//setter params
	public function setName($name){
		$this->name = $name;
	}

	public function getFloors(){
		return $this->floors;
	}

	public function setFloors($floors){
		$this->floors = $floors;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setAddress($floors){
		$this->address = $address;
	}


}

//Instances
$building = new Building('Caswynn Building', 8,'Timog Avenue, Quezon City, Philippines');

//Inheritance and Polymorphism

//extends inherit from the parent class
//for this instance inherit natin yung lahat na nasa condominium

class Condominium extends Building {

	public function printName(){
		return "The name of the condominium is $this->name !";
	}

	//getters(read-only) & setters(write-only)
	//we set from Enzo Condo to Enzo Tower

	//encapsulation, we can hide the data ng mabilisan

	//Activity 4, applies inheritance

	// public function getName(){
	// 	return $this->name;
	// }

	// //setter params
	// public function setName($name){
	// 	$this->name = $name;
	// }

	// public function getFloors(){
	// 	return $this->floors;
	// }

	// public function setFloors($floors){
	// 	$this->floors = $floors;
	// }

	// public function getAddress(){
	// 	return $this->address;
	// }

	// public function setAddress($floors){
	// 	$this->address = $address;
	// }

}

//Instance
$condominium = new Condominium('Enzo Condo',5,'Buendia Avenue, Makati City, Philippines');

//Activity for Session 3

class Person {

	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName) {
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	//methods
	public function printName() {
		return "Your Full Name is $this->firstName $this->lastName";
	}
}

$person = new Person('Senku', 'Zuitt','Ishigami');

class Developer extends Person {

	public function printName(){
		return "Your Name is $this->firstName $this->middleName $this->lastName and you are a developer!";
	}

}

$developer = new Developer('John','Finch','Smith');

class Engineer extends Person {

	public function printName(){
		return "Your are an engineer named $this->firstName $this->middleName $this->lastName!";
	}

}

$engineer = new Developer('Harold','Myers','Reese');

?>